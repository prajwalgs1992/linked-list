#include<iostream>
using namespace std;

struct node
{
    int data;
    node * next;
}*head;

void insertList(int value)
{
    node *temp;
    temp = new node;
    temp->data = value;
    temp->next = NULL;
    if(head == NULL)
    {
        head = temp;
    }else{
        node *s;
        s = head;

        while(s->next != NULL)
            s = s->next;

        s->next = temp;
    }
}

void listSort()
{
    //counter[0] -> stores number of 0s
    //counter[1] -> stores number of 1s
    //counter[2] -> stores number of 2s
    int counter[3] = {0,0,0};
    node *temp = head;
    while(temp != NULL)
    {
        counter[temp->data]++;
        temp = temp->next;
    }
    int i = 0;
    temp = head;

    while(temp != NULL)
    {
        if(counter[i] == 0)
            i++;
        else
            {
                temp->data = i;
                counter[i]--;
                temp = temp->next;
            }
    }

}

void display_list()
{
    node *temp;
    temp = head;
    cout<<"linked list elements are:"<<endl;
    while(temp != NULL)
    {
        cout<<temp->data<<"- >";
        temp = temp->next;
    }
    cout<<"NULL"<<endl;
}

int main()
{
    head = NULL;
    insertList(1);
    insertList(0);
    insertList(1);
    insertList(0);
    insertList(2);
    display_list();
    listSort();
    display_list();
    return 0;
}
