#include<bits/stdc++.h>
using namespace std;

struct node
{
    int data;
    node* next;
};

void display(node *temp);
//like a stack
void push(node **head,int value)
{
    node *new_node;
    new_node = new node;
    new_node->data = value;
    new_node->next = (*head);
    (*head) = new_node;
}

/*function to store elements of both lists*/
void store(node *head1,node *head2,unordered_map<int,int> &m_map)
{
    node *ptr1 = head1;
    node *ptr2 = head2;

    while(ptr1 != NULL || ptr2 != NULL)
    {
        if(ptr1 != NULL)
        {
            m_map[ptr1->data]++;
            ptr1 = ptr1->next;
        }
        if(ptr2 != NULL)
        {
            m_map[ptr2->data]++;
            ptr2 = ptr2->next;
        }
    }
}

node *intersection(unordered_map<int,int> m_map)
{
    node *temp = NULL;
    //cout<<m_map[2]<<endl;
    for(auto itr = m_map.begin();itr != m_map.end();itr++)
    {
        //cout<<itr->first<<endl;
        if(itr->second == 2)
        {
            //cout<<itr->first<<endl;
            push(&temp,itr->first);
        }
    }
    return temp;
}

node *getUnion(unordered_map<int,int> m_map)
{
    node *temp = NULL;
    for(auto itr = m_map.begin();itr != m_map.end();itr++)
    {
        push(&temp,itr->first);
    }
    return temp;
}

void display_union_intersection(node *head1,node *head2)
{
    unordered_map<int,int> m_map;
    store(head1,head2,m_map);
    node *intersection_list = intersection(m_map);
    node *union_list = getUnion(m_map);

    cout<<"intersection list is :"<<endl;
    display(intersection_list);
    cout<<"union list is :"<<endl;
    display(union_list);
}

void display(node *temp)
{
    while(temp != NULL)
    {
        cout<<temp->data<<"->";
        temp = temp->next;
    }
    cout<<"NULL"<<endl;
}


int main()
{
    node *head1 = NULL;
    node *head2 = NULL;
    //list1
    push(&head1, 1);
    push(&head1, 2);
    push(&head1, 3);
    push(&head1, 4);
    push(&head1, 5);
    //list2
    push(&head2, 1);
    push(&head2, 3);
    push(&head2, 5);
    push(&head2, 6);

    cout<<"first list is :"<<endl;
    display(head1);
    cout<<"second list is :"<<endl;
    display(head2);

    display_union_intersection(head1,head2);
    return 0;
}
