#include<iostream>
#include<cstdio>
#include<cstdlib>
using namespace std;

struct node
{
    int data;
    node *next;
}*head;

void reverser()
{
    node *current,*prev,*next;
    current = head;
    prev = NULL;

    while(current != NULL)
    {
        next = current->next;
        current->next = prev;
        prev = current;
        current = next;
    }
    head = prev;
}

void insertList(int value)
{
    node *temp;
    temp = new node;
    temp->data = value;
    temp->next = NULL;
    if(head == NULL)
    {
        head = temp;
    }else{
        node *s;
        s = head;

        while(s->next != NULL)
            s = s->next;

        s->next = temp;
    }
}

void counter(int key)
{
    node *temp =  head;
    int count_t = 0;
    while(temp != NULL)
    {
        if(temp->data == key)
        {
            count_t++;
        }
        temp = temp->next;
    }
    cout<<"the nunmber of times it occurs is:"<<count_t<<endl;
}

void display_list()
{
    node *temp;
    temp = head;
    cout<<"linked list elements are:"<<endl;
    while(temp != NULL)
    {
        cout<<temp->data<<"- >";
        temp = temp->next;
    }
    cout<<"NULL"<<endl;
}

int main()
{
    head = NULL;
    insertList(1);
    insertList(1);
    insertList(3);
    insertList(4);
    insertList(1);
    insertList(6);

    int key;
    cout<<"enter the key!"<<endl;
    cin>>key;

    counter(key);

    display_list();
    reverser();
    display_list();

    return 0;
}
