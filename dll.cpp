#include<iostream>
#include<cstdio>
#include<cstdlib>
using namespace std;

struct node
{
    int data;
    node *next;
    node *prev;
}*start;

class dl_list
{
public:
    void create_list(int value);
    void add_begin(int value);
    void add_after(int value,int pos);
    void delete_element(int value);
    void serach_element(int value);
    void display_dll();
    void countt();
    void revereser();
    dl_list()
    {
        start = NULL;
    }
};


int main()
{
    int choice,element,position;
    dl_list dll;
    while(1)
    {
        cout<<"1.Create Node"<<endl;
        cout<<"2.Add at beginning"<<endl;
        cout<<"3.Add after position"<<endl;
        cout<<"4.Delete"<<endl;
        cout<<"5.Display"<<endl;
        cout<<"6.Count"<<endl;
        cout<<"7.Reverse"<<endl;
        cout<<"8.Quit"<<endl;
        cout<<"Enter your choice : ";
        cin>>choice;
        switch(choice)
        {
            case 1:
                cout<<"enter the element";
                cin>>element;
                dll.create_list(element);
                cout<<endl;
                break;
            case 2:
                cout<<"enter the element";
                cin>>element;
                dll.add_begin(element);
                cout<<endl;
                break;
            case 3:
                cout<<"enter the element";
                cin>>element;
                cout<<"insert element after position:";
                cin>>position;
                dll.add_after(element,position);
                cout<<endl;
                break;
            case 4:
                if(start == NULL)
                {
                    cout<<"list empty"<<endl;
                    break;
                }
                cout<<"enter the element to delete";
                cin>>element;
                dll.delete_element(element);
                cout<<endl;
                break;
            case 5:
                dll.display_dll();
                cout<<endl;
                break;
            case 6:
                dll.countt();
                //cout<<endl;
                break;
            case 7:
                if(start == NULL)
                {
                    cout<<"list is empty"<<endl;
                    break;
                }
                dll.revereser();
                //cout<<endl;
                break;
            case 8:
                exit(1);
            default:
                cout<<"wrong choice"<<endl;
        }
    }
    return 0;
}

void dl_list::create_list(int value)
{
    node *s,*temp;
    temp = new node;
    temp->data = value;
    temp->next = NULL;
    if(start == NULL)
    {
        temp->prev = NULL;
        start = temp;
    }
    else
        {
            s = start;
            while(s->next != NULL)
                s = s->next;

            s->next = temp;
            temp->prev = s;
        }
}

void dl_list::add_begin(int value)
{
    if(start == NULL)
    {
        cout<<"first create a list."<<endl;
        return;
    }
    node *temp;
    temp =  new node;
    temp->prev = NULL;
    temp->data = value;
    start->prev = temp;
    start = temp;
    cout<<"Element inserted"<<endl;
}

void dl_list::add_after(int value,int pos)
{
    if(start == NULL)
    {
        cout<<"first create a list."<<endl;
        return;
    }
    node *temp,*q;
    q = start;
    for(int i = 0;i < pos-1;i++)
    {
        q = q->next;
    }
    temp = new node;
    temp->data = value;
    if(q->next == NULL)
    {
        q->next = temp;
        temp->next = NULL;
        temp->prev = q;
    }
    else
        {
            temp->next = q->next;
            temp->prev = q;
            q->next = temp;
            temp->next->prev = temp;
        }
    cout<<"Element inserted"<<endl;
}

void dl_list::delete_element(int value)
{
     node *temp,*q;
     if(start->data == value)
     {
         temp = start;
         start = start->next;
         start->prev = NULL;
         cout<<"Element deleted"<<endl;
         delete temp;
         return;
     }
     q = start;
     while(q->next->next != NULL)
     {
         if(q->next->data == value)
         {
             temp = q->next;
             q->next = temp->next;
             temp->next->prev = q;
             delete temp;
             cout<<"Element deleted"<<endl;
             return;
         }
         q = q->next;
     }

     if(q->next->data == value)
     {
         temp = q->next;
         delete temp;
         q->next = NULL;
         cout<<"Element deleted"<<endl;
         return;
     }
     cout<<"Element"<<value<<"not found"<<endl;
}

void dl_list::display_dll()
{
    node *q;
    if(start == NULL)
    {
        cout<<"list is empty"<<endl;
        return;
    }
    q = start;
    cout<<"doubly linked list is:"<<endl;
    while(q != NULL)
    {
        cout<<q->data<<"< - >";
        q = q->next;
    }
    cout<<"NULL"<<endl;
}

void dl_list::countt()
{
    node *q = start;
    int counter = 0;
    while(q != NULL)
    {
        q = q->next;
        counter++;
    }
    cout<<"number of elements in the list :"<<counter<<endl;
}

void dl_list::revereser()
{
    node *p1,*p2;
    p1 = start;
    p2 = p1->next;
    p1->next = NULL;
    p1->prev = p2;
    while(p2 != NULL)
    {
        p2->prev = p2->next;
        p2->next = p1;
        p1 = p2;
        p2 = p2->prev;
    }
    start = p1;
    cout<<"list reversed"<<endl;
}
