#include<bits/stdc++.h>
using namespace std;

struct node
{
    int data;
    node *next;
}*head;

void insertList(int value)
{
    node *temp;
    temp = new node;
    temp->data = value;
    temp->next = NULL;
    if(head == NULL)
    {
        head = temp;
    }else{
        node *s;
        s = head;

        while(s->next != NULL)
            s = s->next;

        s->next = temp;
    }
}

void display_list()
{
    node *temp;
    temp = head;
    cout<<"linked list elements are:"<<endl;
    while(temp != NULL)
    {
        cout<<temp->data<<"- >";
        temp = temp->next;
    }
    cout<<"NULL"<<endl;
}

void resolve_dup()
{
    unordered_set<int> s_set;

    node *curr = head;
    node *prev = NULL;
    while(curr != NULL)
    {
        if(s_set.find(curr->data) != s_set.end())
        {
            prev->next = curr->next;
            delete curr;
        }
        else
        {
            s_set.insert(curr->data);
            prev = curr;
        }
        curr = prev->next;
    }
}

int main()
{
    head = NULL;
    //insertList(1);
    insertList(1);
    insertList(3);
    insertList(4);
    insertList(1);
    insertList(6);
    cout<<"before duplicate removal"<<endl;
    display_list();
    resolve_dup();
    cout<<"after duplicate removal"<<endl;
    display_list();
    return 0;
}
