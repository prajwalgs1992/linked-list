#include<iostream>
#include<cstdlib>
#include<cstdio>
using namespace std;

struct node
{
    int data;
    node *next;
}*head;


void insertList(int value)
{
    node *temp;
    temp = new node;
    temp->data = value;
    temp->next = NULL;
    if(head == NULL)
    {
        head = temp;
    }else{
        node *s;
        s = head;

        while(s->next != NULL)
            s = s->next;

        s->next = temp;
    }
}

//void display_list()
//{
//    node *temp;
//    temp = head;
//    cout<<"linked list elements are:"<<endl;
//    while(temp != NULL)
//    {
//        cout<<temp->data<<"- >";
//        temp = temp->next;
//    }
//    cout<<"NULL"<<endl;
//}

void printMiddle()
{
    node *slow_ptr,*fast_ptr;
    slow_ptr = head;
    fast_ptr = head;

    if(head != NULL)
    {
        while(fast_ptr != NULL && fast_ptr->next != NULL)
        {
            fast_ptr = fast_ptr->next->next;
            slow_ptr = slow_ptr->next;
        }
        cout<<"middle element is :"<<slow_ptr->data;
    }
}


int main()
{
    head = NULL;
    insertList(1);
    insertList(2);
    insertList(3);
    insertList(4);
    insertList(5);
    insertList(6);
    //display_list();
    printMiddle();

    return 0;
}
