#include<iostream>
#include<cstdlib>
#include<cstdio>
#include<bits/stdc++.h>
using namespace std;

struct node
{
    int data;
    node *next;
}*head;

void insertList(int value)
{
    node *temp;
    temp = new node;
    temp->data = value;
    temp->next = NULL;
    if(head == NULL)
    {
        head = temp;
    }else{
        node *s;
        s = head;

        while(s->next != NULL)
            s = s->next;

        s->next = temp;
    }
}

bool detectLoop()
{
    unordered_set<node *> s_set;
    while(head != NULL)
    {
        if(s_set.find(head) != s_set.end())
        {
            return true;
        }
        s_set.insert(head);
        head = head->next;
    }
    return false;
}

int main()
{
    head = NULL;
    insertList(1);
    insertList(2);
    insertList(3);
    insertList(4);
    insertList(5);

    head->next->next->next->next->next = head;

    if(detectLoop())
    {
        cout<<"loop detected"<<endl;
    }
    else
    {
        cout<<"theres no loop"<<endl;
    }
    return 0;
}
