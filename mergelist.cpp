#include<iostream>
#include<cstdio>
#include<bits/stdc++.h>
#include<cstdlib>
using namespace std;

struct node
{
    int data;
    node *next;
}*head1,*head2;

node *newNode(int value)
{
    node *tmp = new node;
    tmp->data = value;
    tmp->next = NULL;
    return tmp;
}

void printList(node *n)
{
    while(n != NULL)
    {
        cout<<n->data<<"->";
        n = n->next;
    }cout<<"NULL"<<endl;
}

node *merger(node *n1,node *n2)
{
    if(!n1)
    {
        return n2;
    }
    if(!n2)
    {
        return n1;
    }

    if(n1->data < n2->data)
    {
        n1->next = merger(n1->next,n2);
        return n1;
    }
    else
    {
        n2->next = merger(n1,n2->next);

        return n2;
    }
}

int main()
{
    head1 = newNode(1);
    head1->next = newNode(3);
    head1->next->next = newNode(5);
    printList(head1);

    head2 = newNode(0);
    head2->next = newNode(2);
    head2->next->next = newNode(4);
    printList(head2);

    node *mergeList = merger(head1,head2);

    printList(mergeList);
    return 0;
}
